Yocto Image for Allwinner board setup for Klipper
------------------------------------------------

Build and installation
----------------------

Build image for orange pi pc

    source setup
    bitbake image-for-klipper
    zcat tmp/deploy/images/bananapi-m2zero/image-for-klipper-bananapi-m2zero.wic.gz | sudo dd of=/dev/<microsd> bs=1024M

Klipper is not installed by default. Invoke

    su - klipper -c /var/lib/klipper/setup.sh

to install it.

To change machine to something else, edit build/conf/local.conf and change the following line:

    MACHINE ??= "bananapi-m2zero"


