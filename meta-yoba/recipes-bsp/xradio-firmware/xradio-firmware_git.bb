DESCRIPTION = "Xradio xr819 WiFi firmware"
LICENSE = "CC0-1.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/${LICENSE};md5=0ceb3372c9595f0a8067e55da801e4a1"

PV = "git${SRCPV}"

SRCREV = "64e1c5db04e8c5b3bf3ae79ef659b68004645d89"

COMPATIBLE_MACHINE = "orange-pi-zero"

SRC_URI = "git://github.com/armbian/firmware.git;protocol=https;branch=master"

S = "${WORKDIR}/git"

do_install() {
    install -d ${D}${base_libdir}/firmware/xr819
    install -m 0644 ${S}/xr819/boot_xr819.bin ${D}${base_libdir}/firmware/xr819/
    install -m 0644 ${S}/xr819/sdd_xr819.bin ${D}${base_libdir}/firmware/xr819/
    install -m 0644 ${S}/xr819/fw_xr819.bin ${D}${base_libdir}/firmware/xr819/
}

FILES:${PN} = "${base_libdir}/*"

PACKAGES = "${PN}"
