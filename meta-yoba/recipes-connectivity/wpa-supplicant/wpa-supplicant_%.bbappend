FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
FILESEXTRAPATHS:prepend := "${TOPDIR}/../files/wpa-supplicant:"
DEPENDS:remove = "dbus"

do_install:append() {
    rm -rf ${D}/${sysconfdir}/dbus-1
}
