SUMMARY = "Realtek out-of-tree kernel driver for rtl8189fs"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://include/autoconf.h;startline=1;endline=18;md5=a9a640a65da577508327c26a20da868a"

inherit module

PV = "git${SRCPV}"

SRC_URI = "git://github.com/jwrdegoede/rtl8189ES_linux.git;branch=rtl8189fs;protocol=https \
	file://0001-Add-module-install.patch \
	"
SRCREV = "476020109b3841421af289a7b78c7a25b0c45fac"
S = "${WORKDIR}/git"

EXTRA_OEMAKE += " \
    CONFIG_RTL8189FS=m \
    KSRC=${STAGING_KERNEL_DIR} \
    "
KERNEL_MODULE_AUTOLOAD += "8189fs"
