FILESEXTRAPATHS:prepend := "${THISDIR}:${THISDIR}/files:"
SRC_URI += "file://0001-uart2-bananapi-m2z-enable.patch \
            file://0002-usb-bananapi-m2z-host.patch \
            file://0003-spi-bananapi-m2z.patch \
            file://ondemand-governor.cfg \
            file://usb-musb-hdrc.cfg \
            file://usb-acm.cfg \
            file://thermal.cfg \
           "

SRC_URI:append:orange-pi-zero = "file://0001-thermal-orangepi-zero.patch"
