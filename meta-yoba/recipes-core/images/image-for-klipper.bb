DESCRIPTION = "Klipper image minimal"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit core-image
inherit extrausers

EXTRA_USERS_PARAMS = " \
    usermod -p '' root ; \
    usermod -p '' klipper ; \
"

IMAGE_INSTALL += " \
    packagegroup-core-buildessential \
    kernel-modules \
    python3 \
    python3-dev \
    python3-pip \
    python3-cffi \
    python3-pyserial \
    ncurses \
    ncurses-dev \
    libusb-compat \
    libusb-compat-dev \
    libusb1 \
    libusb1-dev \
    chrony \
    libffi \
    sudo \
    openssh \
    openssh-sftp-server \
    packagegroup-avr \
    avrdude \
    stm32flash \
    wpa-supplicant \
    iw \
    kernel-module-rtl8192cu \
    linux-firmware-rtl8192cu \
    wireless-regdb-static \
    git \
    cmake \
    swig \
    swig-dev \
    klipper-scripts \
    picocom \
    socat \
    stm32flash \
    rlwrap \
    usbutils \
    spitools \
    tmux \
    ssh-custom-keys \
"

IMAGE_INSTALL:append:orange-pi-zero = "xradio xradio-firmware"

KERNEL_MODULE_AUTOLOAD += "rtl8192cu"

enable_sudo_group() {
    #This magic looking sed will uncomment the following line from sudoers:
    # %sudo   ALL=(ALL:ALL) ALL
    sed -i 's/^#\s*\(%sudo\s*ALL=(ALL:ALL)\s*ALL\)/\1/'  ${IMAGE_ROOTFS}/etc/sudoers
}

ROOTFS_POSTPROCESS_COMMAND += "enable_sudo_group;"
