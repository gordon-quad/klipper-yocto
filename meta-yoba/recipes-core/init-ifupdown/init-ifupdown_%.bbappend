FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://powersave_off"

do_install:append () {
	install -m 0755 ${WORKDIR}/powersave_off ${D}${sysconfdir}/network/if-up.d
}

