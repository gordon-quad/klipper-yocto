SUMMARY = "Klipper setup and init scripts"
HOMEPAGE = "http://klipper3d.org"

LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=73f1eb20517c55bf9493b7dd6e480788"

PV = "1.0"

inherit update-rc.d useradd

INITSCRIPT_NAME = "klipper-server"
INITSCRIPT_PARAMS = "start 80 2 3 4 5 . stop 01 0 6 1 ."

SRC_URI = "file://init \
           file://setup \
           file://run \
           file://LICENSE \
          "

S = "${WORKDIR}"

FILES:${PN} += "${sysconfdir} ${localstatedir}"

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "--system --home ${localstatedir}/lib/klipper/ -M -g nogroup -G dialout,sudo klipper"

do_install() {
    install -d ${D}$/var/lib/klipper
    install -Dm 0755 ${WORKDIR}/setup ${D}${localstatedir}/lib/klipper/setup.sh
    install -Dm 0755 ${WORKDIR}/run ${D}${localstatedir}/lib/klipper/run.sh
    install -Dm 0755 ${WORKDIR}/init ${D}${sysconfdir}/init.d/klipper-server
}

pkg_postinst:${PN}:append () {
    chown -R klipper $D${localstatedir}/lib/klipper
}
