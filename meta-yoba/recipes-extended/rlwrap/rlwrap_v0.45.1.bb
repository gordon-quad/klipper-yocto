SUMMARY = "GNU readline wrapper"
HOMEPAGE = "https://github.com/hanslub42/rlwrap"
LICENSE = "GPL-2.0-only"

LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

inherit autotools gettext perlnative

DEPENDS = " \
    readline \
"

SRC_URI = "git://github.com/hanslub42/rlwrap.git;protocol=https;branch=master"
SRCREV = "5aa2609a7e4c76f37c721110bcec312d3efa7e2d"
S = "${WORKDIR}/git"
PV = "v0.45.1"
